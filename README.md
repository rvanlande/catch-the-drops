# catch-the-drops

## TODO

* background screen

## Déploiement

### HTML5

* export html5

```
Project -> Export -> HTML5 -> Export project
```

Note :  

- `Project -> Export -> Add` : pour ajouter un modèle d'export (html5, android, ...)
- dans la fenêtre ``Project -> Export`, `Runnable -> off` permet d'activer le bouton `Èxport project`, `Runnable -> on` permet d'activer le bouton permettant de déployer en live sur la plateforme cible.  
- renommer le fichier `.html` en `ìndex.html`

* start web server

```
`$ docker run --rm -dit --name catch-the-drops-server -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd:alpine
```


