extends Node

signal start_pressed

func _ready():
	$LeftSprite.hide()
	$RightSprite.hide()

func _on_StartButton_pressed():
	emit_signal("start_pressed")
