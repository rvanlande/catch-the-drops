extends Node

export (PackedScene) var Drop
var score

func _ready():
	randomize()
	
func _on_Menu_start_pressed():
	start_game()

func _on_DropTimer_timeout():
	spawn_drop()

func _on_Bucket_hit():
	drop_caught()

func _on_FloorArea_body_entered(body):
	game_over()
	
func _on_AccelerateTimer_timeout():
	if $DropTimer.wait_time > 0.6:
		$DropTimer.wait_time -= 0.2

	
# ---------------------------------------------------

func start_game():
	score = 0
	$Hud/Message.hide()
	$Hud/ScoreLabel.text = str(score)
	$Hud/ScoreLabel.show()
	$Bucket.position = $Bucket/StartPosition.position
	$Bucket.show()
	$DropTimer.wait_time = 2
	$DropTimer.start()
	$AccelerateTimer.start()
	$Hud/StartButton.hide()
	$RainMusic.play()
	$Hud/LeftSprite.show()
	$Hud/RightSprite.show()
	
func spawn_drop():
	$DropPath/DropPathFollow.set_offset(randi())
	var drop = Drop.instance()
	$FloorArea.connect("body_entered", drop, "_on_game_over")
	drop.position = $DropPath/DropPathFollow.position
	add_child(drop)
	
func drop_caught():
	score += 1
	$Hud/ScoreLabel.text = str(score)
	$DropMusic.play()
	
func game_over():
	$DropTimer.stop()
	$Hud/Message.text = "Game Over"
	$Hud/Message.show()
	$Hud/StartButton.show()
	$Bucket.hide()
	$RainMusic.stop()
	$Hud/LeftSprite.hide()
	$Hud/RightSprite.hide()

