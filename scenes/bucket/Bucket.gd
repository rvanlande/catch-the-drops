extends Area2D
signal hit

export (int) var SPEED
export (NodePath) var StartPos
var screen

func _ready():
	hide()
	screen = get_viewport_rect().size
	

func _process(delta):
	
	var velocity = Vector2(0,0)
	
	if Input.is_action_pressed("ui_left") or Input.is_action_pressed("move_left"):
		velocity.x -= 1
	elif Input.is_action_pressed("ui_right") or Input.is_action_pressed("move_right"):
		velocity.x += 1
		
	velocity = velocity.normalized()
	
	if velocity.length() > 0:
		position += velocity * SPEED * delta
		position.x = clamp(position.x, 0, screen.x)
	


func _on_Bucket_body_entered(drop):
	drop.queue_free()
	emit_signal("hit")

